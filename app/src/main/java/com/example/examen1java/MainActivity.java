package com.example.examen1java;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //VARIABLES
    private Button btnEntrar, btnSalir;
    private EditText txtUsuario;
    //componentes
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSalir = findViewById(R.id.btnSalir);
        btnEntrar = findViewById(R.id.btnEntrar);
        txtUsuario = findViewById(R.id.txtUsuario);

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {  //programacion de BOTON
                if (txtUsuario.getText().toString().matches("")) {
                    //Falto capturar nombre
                    Toast.makeText(MainActivity.this, "Favor de ingresar su nombre", Toast.LENGTH_SHORT).show();
                } else {
                    String strUsuario = txtUsuario.getText().toString();
                    Bundle bundle = new Bundle();
                    bundle.putString("usuario",strUsuario);
                    Intent intent = new Intent(MainActivity.this, Examen1JavaActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });


        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                AlertDialog.Builder confirmar = new AlertDialog.Builder(btnSalir.getContext());
                confirmar.setTitle("Recibo de Nomina");
                confirmar.setMessage("¿Desea regresar?");
                confirmar.setPositiveButton("Confirmar", (dialogInterface, which) -> finish());
                confirmar.setNegativeButton("Cancelar", (dialogInterface, which) -> finish());
                confirmar.show();
            }

        });

    }
}