package com.example.examen1java;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;


public class Examen1JavaActivity extends AppCompatActivity {
    private EditText  txtBase, txtAltura;
    private Button btnCalcular, btnLimpiar, btnRegresar;
    private TextView txtArea,txtPerimetro, lblUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.examen1java);
        iniciarComponentes();

        // Obtener los datos del MainActivity
        Bundle datos = getIntent().getExtras();
        String usuario = datos.getString("usuario");
        lblUsuario.setText(usuario);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validarCampos()) {
                    calcularNomina();
                } else {
                    mostrarToast("Por favor, completa todos los campos");
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });
    }


    private void iniciarComponentes(){
        txtArea = findViewById(R.id.txtArea);

        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        lblUsuario = findViewById(R.id.lblUsuario);
        txtAltura = findViewById(R.id.txtAltura);
        txtPerimetro = findViewById(R.id.txtPerimetro);
        txtBase = findViewById(R.id.txtBase);

    }

    private boolean validarCampos() {
        String base = txtBase.getText().toString();
        String altura = txtAltura.getText().toString();

        return !base.isEmpty() && !altura.isEmpty();
    }

    private void calcularNomina() {
        // Obtener los valores de entrada de los EditTexts
        float base= Float.parseFloat(txtBase.getText().toString());
        float altura = Float.parseFloat(txtAltura.getText().toString());


        Examen1Java examen1Java = new Examen1Java( base, altura);

        // Realizar los cálculos necesarios en el objeto reciboNomina
        float area =  examen1Java.CalcularArea();
        float perimetro =  examen1Java.CalcularPerimetro();

        // Mostrar los valores calculados en los TextViews
        txtArea.setText(String.valueOf(area));
        txtPerimetro.setText(String.valueOf(perimetro));
    }

    private void limpiarCampos() {
        txtArea.setText("");
        txtPerimetro.setText("");
        txtBase.setText("");
        txtAltura.setText("");
    }
    private void regresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setMessage("Regresar al Menú Principal?");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        confirmar.show();
    }
    private void mostrarToast(String mensaje) {
        Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT).show();
    }
}


