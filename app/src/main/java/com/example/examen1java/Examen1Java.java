package com.example.examen1java;

public class Examen1Java {
    //DATOS DECLARADOS
    private float base,altura;


    //CONSTRUCTOR
    public Examen1Java(float b, float a){
        this.base = a;
        this.altura = b;
    }


    public float CalcularArea(){
        float area = altura *base;
        return area;
    }

    public float CalcularPerimetro(){
        float Perimetro = ((altura+base)*2);
        return Perimetro;

    }


}
